//@ts-check

/**
 *
 * @param {Node} node 
 * @param {boolean} isLowestMargin
 * 
 * @returns {Node}
 */
let matrixReduction = (node, isLowestMargin = false) => {
    let { matrix, weight, Hordes, from, to } = node;
    let { tempMatrix, minRow } = matrix.reduce((res, row) => {
        let min = Math.min(...row);
        res.tempMatrix.push(row.map((item) => (item - min)));
        res.minRow.push(min);
        return res;
    },
        {
            tempMatrix: [],
            minRow: []
        });

    let minCol = [...tempMatrix[0]];
    tempMatrix.slice(1).forEach(row => {
        row.forEach((item, index) => {
            minCol[index] = Math.min(minCol[index], item);
        }
        );
    });

    matrix = tempMatrix.map(row => {
        return row.map((item, index) => (item - minCol[index]));
    });

    if (isLowestMargin) {
        /**
        * @param {Array<Number>} arr
        * 
        * @returns {Number}
        */
        let sum = (arr) => arr.reduce((res, item) => (res + item), 0);
        weight += sum([...minCol, ...minRow]);
    }

    // @ts-ignore
    return {
        matrix,
        Hordes,
        weight,
        from,
        to
    }
}

/**
 *
 * @param {Array<Array<number>>} matrix
 * 
 * @returns {Delta}
 */
let reatingMaxPenalty = (matrix) => {


    let penaltys = matrix.reduce(
        /** 
        * @param {Array<Delta>} penaltys */
        (penaltys, row, i) => {

            penaltys.push(...row.reduce(
                /**
                 * @param {Array<Delta>} penaltys */
                (penaltys, item, j) => {

                    if (item === 0) {
                        function findMinCol(i, j) {
                            let penaltys = [];
                            matrix.forEach((row, index) => {
                                if (i != index) penaltys.push(row[j]);
                            });
                            return Math.min(...penaltys);
                        }
                        let penalty = findMinCol(i, j) + Math.min(...row.slice(0, j), ...row.slice(j + 1));
                        /**
                         * @type {Delta}
                         */
                        let delta = {
                            i, j, penalty
                        };
                        penaltys.push(delta);
                    }
                    return penaltys;
                }, []));

            return penaltys;
        }, []);


    // @ts-ignore
    return penaltys.reduce((neededDelta, delta) => {
        if (neededDelta.penalty < delta.penalty) {

            neededDelta = delta;
        }
        return neededDelta;
    }, { penalty: 0 });
}

/**
 *
 * @param {Node} node 
 * 
 * @returns {[Node,Node]}
 */
let split = (node) => {

    let { matrix, Hordes, weight, from, to } = node;

    let horde = reatingMaxPenalty(matrix);

    matrix[horde.i][horde.j] = Infinity;

    /**
     * @type {Node}
     */
    // @ts-ignore
    let withoutHorde = {
        matrix: [...matrix],
        Hordes,
        weight: weight + horde.penalty,
        from,
        to
    };

    withoutHorde = matrixReduction(withoutHorde);

    let reverseHordeIndexes = {
        from: from.indexOf(to[horde.j]),
        to: to.indexOf(from[horde.i])
    }

    if (reverseHordeIndexes.from >= 0 && reverseHordeIndexes.to >= 0) {
        matrix[reverseHordeIndexes.from][reverseHordeIndexes.to] = Infinity;
    }

    let cropMatrix = matrix.map(row => [...row.slice(0, horde.j), ...row.slice(horde.j + 1)]);
    cropMatrix = [...cropMatrix.slice(0, horde.i), ...cropMatrix.slice(horde.i + 1)];

    /**
     * @type {Node}
     */
    // @ts-ignore
    let withHorde = {
        matrix: cropMatrix,
        Hordes: [...Hordes, { from: from[horde.i], to: to[horde.j] }],
        weight: weight,
        from: [...from.slice(0, horde.i), ...from.slice(horde.i + 1)],
        to: [...to.slice(0, horde.j), ...to.slice(horde.j + 1)]
    }

    withHorde = matrixReduction(withHorde, true);

    console.log('with', withHorde);
    console.log('without', withoutHorde);

    if (withHorde.weight < withoutHorde.weight) {
        return [withHorde, withoutHorde];
    }
    return [withoutHorde, withHorde];
}

exports.lib = { mR: matrixReduction, rMP: reatingMaxPenalty, split };
