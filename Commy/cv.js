const { lib } = require("./functions.js");

let X = Infinity;

let M = { 
    matrix: [
        [X,5,6,4,X],
        [4,X,3,5,3],
        [6,4,X,5,6],
        [7,X,2,X,3],
        [5,2,8,4,X]
    ],
    Hordes: [],
    from: [1,2,3,4,5],
    to: [1,2,3,4,5],
    weight: 0
}

let M1 = [
    [X ,20,18,12,8 ],
    [5 ,X ,14,7 ,11],
    [12,18,X ,6 ,11],
    [11,17,11,X ,12],
    [5 ,5 ,5 ,5 ,X ]
];

let M3 = [
    [X,2,2,7,X],
    [8,X,5,4,2],
    [3,5,X,5,6],
    [5,X,8,X,4],
    [5,8,4,3,X]
]


M.matrix = M3;

let queue = [lib.mR(M, true)];

queue.insert = function(arr) {
    let i = 0;
    while(arr.length) {
        if ( i >= this.length || arr[arr.length-1].weight > this[i].weight) {
            this.splice(i,0,arr.pop());
        }
        i++;
    }

}

 while( queue[queue.length - 1].matrix.length > 1) {
    let node = queue.pop();
    console.log('split', node)
    queue.insert(lib.split(node)); 
    console.log('\n------------------------------------------------------------\n')
}

let answer = queue.pop();

let path = answer.Hordes.reduce( (res, item) =>  {
    res[item.from] = item.to;
    return res;
}, []);

path[answer.from[0]] = answer.to[0];

let pathLength = path.length;
let node = path[1];
let message = node;

while(--pathLength) {
    node = path[node];
    message+='->'+node;
}
console.log(message, '\nlength - ', answer.weight);