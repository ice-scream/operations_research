interface Horde {
    from: number;
    to: number;
}

interface Node {
    matrix: Array<Array<number>>;
    Hordes: Array<Horde>;
    weight: number;
    from: Array<number>;
    to: Array<number>;
}

interface Delta {
    i: number;
    j: number;
    penalty: number;
}